FROM openjdk:8-jre-alpine
WORKDIR /home/gitlab-runner/target/
COPY . /
EXPOSE 8080
ENTRYPOINT ["java","-jar","/java-app-1.0-SNAPSHOT.jar"]
